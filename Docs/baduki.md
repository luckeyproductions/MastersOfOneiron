### Comparison

In sound, Baduki somewhat resembles a mix between Italian and Swahili. In writing it's closer to Devanagari or Mongolian, written left-to-right, with lines and syllables stacked from below.

#### Pronunciation

Glyph | Orthographic | English | [IPA](https://www.ipachart.com/) |
------|------|---------|-----|
![b](Glyphs/b.png)| b | **b**o**b** |**b**
![a](Glyphs/a.png)| a | popc**aa**n |**a**
![à](Glyphs/à.png)| à | d**a**rt    |**ɑ**
![d](Glyphs/d.png)| d | **d**im     |**d**
![u](Glyphs/u.png)| u | r**ou**ge   |**u**
![ù](Glyphs/ù.png)| ù | b**u**d     |**ɞ**
![k](Glyphs/k.png)| k | **c**old    |**kʼ**
![i](Glyphs/i.png)| i | k**e**y     |**i**
![ì](Glyphs/ì.png)| ì | b**i**t     |**ɪ**
![p](Glyphs/p.png)| p | **p**ost    |**p**
![e](Glyphs/e.png)| e | m**a**y     |**e**
![è](Glyphs/è.png)| è | g**e**t     |**æ**
![o](Glyphs/o.png)| o | h**o**t     |**o**
![g](Glyphs/g.png)| g | **g**or**g**on|**ɢ**
![j](Glyphs/y.png)| y | **y**o**y**o|**j**
![t](Glyphs/t.png)| t | **t**op     |**ʈ͡ʂ**
![h](Glyphs/h.png)| h | **h**ealth  |**ɦ**
![s](Glyphs/s.png)| s |  | **s**
![z](Glyphs/z.png)| z |  | **z**
![n](Glyphs/n.png)| n |  | **n**
![m](Glyphs/m.png)| m |  | **ɱ**
![r](Glyphs/r.png)| r |  | **r**
![f](Glyphs/f.png)| f |  | **f**
![l](Glyphs/l.png)| l |  | **l**
![w](Glyphs/w.png)| w |  | **w**


##### [**Ufdu bodune o gro ti jo; gawàki bogane o jidukin lotogane groduplitf**](https://raw.githubusercontent.com/LucKeyProductions/MastersOfOneiron/master/Resources/Samples/Voice/UfduBodune.ogg)

#### Baduki

![Baduki](Words/baduki.png)

Syllable | Meaning
---------|---------
ba       | word/syllable/sound/bla
du       | collection
ki       | the

Baduki is the language of the Èkèlpliths


---------------------------------

#### Èkèlplitfkin

Syllable | Meaning
---------|---------
èk       | two
èl       | blade
plitf    | rizen
kin      | the (plural)

The Èkèlplitfkin - also shortened to Èlki(n) - are very proud of the fact they manage to walk. Walking required both their heelfins as well as their kneefins (èln). 

#### Brèkèlega

Syllable | Meaning
---------|---------
brèk     | martial
èl       | blade
ega      | dance
e        | feeling/awareness
ga       | move

The Brèkèlega dance utilizes the Èkèlplitfs' finblades as weapons.


---------------------------------

## Syllables

### Numbers (duban)

Since Èkèlplitfs have eight fingers in total, it's in their nature to count and calculate things in - what we call - nibbles and bytes.


Syllable | Meaning
---------|---------
uf	     | zero / none / nothing
ut	     | one 
èk       | two
ur	     | three
art      | four
|
oz	     | five?
zik      | ten?
uzu      | fifteen? (010)
zàt      | twenty / many?

### Postfixes

 Postfix | Meaning
---------|---------
-N	     | plural
-KI      | the (unique, one and only, defining one lit. egg atom, proverbial: spark of creation)
-NE	     | verbal / doing
-NÌ      | doubtful doing
-NI      | not doing / refusing
-R       | possessive

### Prefixes

Prefix | Meaning
-------|---------
NI-    | negating / opposite

### Pronouns

 English  | Nominative / Reflexive | Possessive / Containing
----------|------------------------|------------
I/me/mine               | MI       | MII
we/us/our               | MIN      | MINI
you/your (singular)     | YU       | YUI
you/your (plural)       | YUN      | YUNI
he/she/him/her/his/hers | DÈ       | DÈI
they/them/their         | DÈN      | DÈNI
this/that/it/its        | TÈ       | TÈI
these/those/them/their  | TÈN      | TÈNI

### Roots

Syllable | Meaning or common implication
---------|---------
A | sound / aum / vowel
À | interruption / stitch / knot
Y | soul
I | atom / grain / particle / little / bit
B | collision / consonant / interruption
R | vibration / wave / resonance
E | feeling/awareness/conscience
È | unknown / dunno
O | and / plus / more / hand / offer
F | light
G | weight / mass
D | persistent / still
Z | next / sideways / left
Ù | huh? / what?
U | or / other
H | breath / air / gap
T | level / layer
K | Egg/Seed


-----------------------

## Assembled words

Word        | Meaning
------------|---------
Mu          | World
Muki        | The world
Ya          | thought
Yadu        | Philosophy
Kudu        | Science
Yaduyi      | Philosopher
Kuduyi      | Scientist
Zù          | right?
Iki         | part / piece
Ti          | am / is / are
Tà          | material / resource / thing
Wa          | source / from
Waki        |
Li          | will / is going to / are going to
Liki        | fate / destiny
Muliki      | worldly fate
Dumuliki    | worldly fate
Zi          | step
Ba          | word / syllable
Bo          | great / good / beautiful
Br          | damage / result / effect
Hu          | fear
Ho          | respect / hi 
Èl          | blade
KEL         | truce (lit. cracked blade)
KELKI       | Satya Yuga (Kalki)
ELK         | cut
ELKNE       | to cut
To          | with / along / towards / further / downstream (lit. another layer)
Lo          | aim / goal / target
Ikilo       | approximate aim (name of mythical character)
Ri          | tooth
Ridu        | dentition
Èlri        | Serrated greaves 
Yi          | Individual / entity
Yimu        | Subjective reality
Yo          | special / scarce / important
La          | marks the beginning of a question (will, what, where, etc.)
Mìk         | Here
Màk         | There
Mìkmàk      | Occasionally (here and there / now and then)
Pl          | push/pull/force
Ipl         | careful
Sèr	        | sharp
NIZ         | previous / right
BRO         | then / result / outcome
BRU         | else
BRUT        | pain / suffering / burden
BRUF        | meaningless
Brèk        | martial / fight / split / bifurcation
DA          | thank
DAN         | thanks for / gratitude
DU          | collection
RO          | power / cycle / reign / rule
**!**HAM    | exchange
PLITF       | against / towards source / upstream / carried
UFBA        | silence
BANI        | being quiet
BADUNE	    | singing / telling a story
IDU         | dust
TADU        | stock / belongings
Badutà      | book / letter / scriptum
Badukità    | dictionary
Baduwayi    | singer / poet / writer / story teller / orator / wordsmith
HONE        | to greet
WANE        | to create / provide
BOWAYIKI	| The supplier of goodness
BAHAM       | conversation
Badu        | language / song / story (any composition of words and/or sounds)
TADU        | stockpile / possessions
EPLITF      | empathy
EDU         | atmosphere
EWA         | heart
ENE         | to feel / to notice / to being aware (of) / to know
EYO         | care
YOE         | love
YOEBADU 	| love song
BOBA 	    | compliment / praise
BOBADU 	    | canticle
BRÈKBA 	    | insult
BRÈKBADU    | diss rap
DANBADU     | paean
BRÈKYI      | fighter
GAYI 	    | mover
GRO         | ground
GROTA 	    | soil
GROTO       | groundwards
GROTOKI     | gravity
FTO		    | in(ward)
FLO         | out(ward)
FLOTO       | liquid
FLOTOTA     | drink / beverage
FROPFLOTOTA | frop root beer
EF          | sight
EFNE        | to see
EFNETA      | eye
EFYI        | scout / sentry / watcher
FE          | peace
FENE        | meditating / resting
FEYI        | peacekeeper
FWA		    | light source
FWAKI	    | the nucleus
FWATA       | torch
FUF         | darkness
FUFHU       | fear of the dark
TAWAGRO     | agriculture
WAGROTA     | harvest / crops (ground-sourced)
GRODU 	    | platform
GA          | movement
GAF         | air/wind
GAFGAWA     | windmill
GROGAYI     | digger
EGAYI 	    | dancer
WAYI        | creator / provider
UTTAWAYI 	| specialist / craftsman
ÈLWAYI      | blacksmith
DUWAYI      | collector
ÈLWANE      | smithing
HAMDU 	    | market
HAMNE 	    | to trade
HAMYI 	    | trader
DUYI 	    | member
YIDU 	    | group
YIDUKI   	| the clan
HUYI        | wimp
HUWAYI      | creep
YOYO 	    | magic
YOYONE	    | casting
YOYOWA      | mana / qi
YOYOTA      | ectoplasm
HUWAYI      | boogie man
YOYOHUWAYI  | ghost
YOYOYI 	    | mage
YOYOKI 	    | the great work
YOYOBA 	    | spell
YOYOBOBA    | blessing
YOYOBRUTBA	| curse
Yozàt       | very special (exclamation of surprise)
Huzàt       | many scares (used both to scare and as a response to it)
DATA 	    | gift (thank object)
SERRI 	    | wolf
BRÈKSERRI 	| war dog
UFHU 	    | fearless / bravery
UFHUYI	    | hero / fool / dare devil
URHO 	    | honor / admiration
ROYI        | ruler (e.g. king)
BAWA		| mouth
LOTO		| path
LOTOGA	    | journey
LOTOGANE    | traveling
LOTOGAYI    | traveler
LOBETFRI    | toothbrush (lit. leads to clean teeth)
PLITFE	    | balance
GRODUWANE	| platforming
GANE		| moving
BAWANE	    | making sound / informal speaking
UFNE		| to rest
EUFNE    	| navel-gazing / being bored
EUTNE       | meditation
UFNEYI	    | idle person
ÈKNE		| dividing
UTNE		| uniting
SERRIGANE	| dog riding
BRÈKSERRIGAYI | dog riders
GAWA		| motor
GAWAKI	    | time
GANITO      | New (unmoved/unhandled object)
ONE		    | joining / adding / increasing
YÙ          | sweet, pleasant
YÙM         | fruit
KU          | idea (egg of alternatives)
Kuyàne      | I was thinking... / I have an idea...
Wàku        | Mind
Wàkuwàku    | Akashic records
OYI	        | guest / newcomer (the person that joined me/us)
RINE		| biting
BRÈKRINE    | chewing / munching
BOGA		| fast / quick

-----------------------

## Sentences

**Serrin zo Ô nizo; min li brèkne ten**  
Dogs left and right; we will fight them

**Den hone min**  
They're greeting us

**Mi gane**  
I'm moving/going

**Min ti serrin!!!**  
We are wolves!!!

**Dan egaham**  
Thanks for your cooperation / It was a pleasure working together / _Thanks for this dance_

**Ti bo**  
Take care / be nice / you're good

**Mi hone yur**  
I greet you

**Mir hon ti yur**  
You have my respect

**Bu te egani, bru te badu**  
If it don't dance... else it's a story

