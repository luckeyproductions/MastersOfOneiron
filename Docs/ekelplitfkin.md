
# The Ekelplitfs
Ekelplitfkin are blueish cyclopes that live on the light side of the temperate mattershell. Their language is called [Baduki](baduki.md).  
Their [culture](ekelplitfculture.md) could be discribed as being tribal magidustrial fropconomy. They're fond of gadgets and technology but afraid of the dark.  
The Ekelplitfkin (as they call themselves) cultivate frop, both as their main food source and construction material. From its root they brew a kind of root beer they call Fropfloto(ta(n)).

Ekelplithfs are strong creatures and superb swimmers. They can cross the sheet of water in between platforms and dive to the other side. Although heavy equipment needs an elevator or bridge to come along, light equipment only slows them down a bit.

##### Reasons to visit the outside despite fear
 
The main reason is that on the outside of the planes there grows a special kind of mushroom which helps the Ekelplitfs enter the summoning/conversion state.
In this state more Ekelplitfs can be summoned or unfriendly Ekelplitfs can be converted. Sometimes there are also strategic stealthy covert ops reasons, and then there are the nightmare hunters; slightly twisted Ekelplitfs that go to the outside for sport.

## Technology

Technologies are abilities, items, vehicles and structures that can be unlocked through [science](ekelplitfscience.md).

### Spells


Level 1:

- Burn: Concentrates energy at the palm of one's hands
- Eyoki: Restores lost health
- Illuminate: Lights the path by focusing stamana into a gem

### Light Equipment & Items

These can be placed in an Ekelplitf's inventory, which by default is [2x1] in size. Items can be rotated as they are moved. <!-- Those that can be used are marked with a U, equipment is marked with an E, resources with an R. Some buildings also have inventory slots. -->

Creature | Equipment slots
---|---
**Ekelplitf** | Hands, Back, Outfit (default: _kilt_), Waist, Legs
**Seri** | Back
**Mule** | 2x Back

#### 1 Square

Resource | Applications
---|---
**Frop** | Food, Fiber, Fropcrete, Root beer, Oil and Cellulose
**Ground** | Modifies platform shape, 16 per square, also construction material

Usable | Effect
---|---
**Fropcake** | Restore health and energy
**Fropflotota** | Energy potion (bubbles go down)

Equipment | Slot | Effect
---|---|---
**Palm Amp** | Hands | Increases max spell level by 1

#### 2 Squares

Equipment | Slot | Effect
---|---|---
**Construction tools** | Hands | Enables construction beyond basic, faster digging
**Robe** | Outfit | Increases max spell level by 1
**Backpack** | Back | Increases carrying capacity, adds 8 (2x4) inventory slots >> _rack & bucket_
**Jet** | Back | Faster in water, on wheels or in the air, runs on _fropoil_
**Elri** | Legs | Serrated greaves (increase [Brekelega](baduki.md#Brekelega) damage and Frop harvest speed)
**Control belt** | Waist | Mobile control center for mechanized structures, disabled by occupied _hands_ slot.

#### 3 Squares

Equipment | Slot | Effect
---|---|---
**Oar** | Hands | For moving platforms, also a weapon.
**Rocket pack** | Back | Functions as both _jet_ and _balloon suit_.
**Battery** | Back | Stores energy

#### 2x2 Squares

Equipment | Slot | Effect
---|---|---
**Body armor** | Outfit | Defence
**Buckler blades** | Hands | Weapon and armor in one
**Balloon suit** | Outfit | Enables flight
**Rack** | Back | Adds 18 (3x6) non-resource inventory slots
**Bucket** | Back | Adds 12 (2x6) resource slots
**Propeller** | Back | Faster in water, on wheels or in the air >> _jet_

### Heavy equipment & Vehicles

Only one of these per Ekeleplitf, disables [Brekelega](baduki.md#Brekelega), swimming and climbing, always equipped (or stored in a structure).

Equipment | Function | Slots | Upgrades to
---|---|---|---
**Blade wheels** | Segway kneeblade roller skates (fast land movement) | Legs
**Extensible tractor** | Somewhat fragile | _Hands & Legs_ | _Mechanised turtle_
**Mechanised Turtle** | Armored and Amphibious (needs ramp) | _Hands & Legs_ |

#### Tractor extensions

Front | Function
---|---
None | Faster movement
Combine | Enables harvesting
Excavator | Enables digging
Printer | Harvester and construction vehicle in one
Devastator | Amplified focused energy beam

Back | Function | Upgrades to
---|---|---
**Cart** | (carry items, can also be pulled by mules and Ekeplitfs, not amphibious) | _Hopper_
**Hopper** | Carries resources, up to 3 allowed, requires conveyor belt | _Tube_ 
**Tube** | Greater capacity than hopper, up to 5 allowed |

  
### Structures

#### Basic

Name | Function | Occupants | Inventory | Upgrades to 
---|---|---|---|---
**Altar** | Summons level 0 Ekelplithfs from sleeping souls using mushrooms and the burning of _frop_ | 4 | 2 | _Temple_
**Workbench** | For creating the most basic of tools | 2 | 2 | _Workshop_
**Burrow** | A place to rest | 1 | 1 | _Tent_
**Tent** | A more efficient place to rest | 2 | 3 | _Abode_
**Pool** | Has to be occupied to have centralized control over a platform. When it is not, only individual Ekelplitfs can be ordered around | 1 | 0 | _Bathtub_
**Seat** | Better rowing | 1 | 0 | _Peddler_
**Drawbridge** | Ramp and crossing | 1 | 0 | _Extendable bridge_

#### Sophisticated

Name | Function | Occupants | Inventory | Upgrades to 
---|---|---|---|---
**Bathtub**| Pool with morale bonus | 1 | 0 | _Hot tub_
**Abode** | Heals garrisoned units and protects them, stacks to 2 | 4 | 8 | _Spire_
**Workshop** | Constructs equipment and vehicles | 3 | 9 | _Factory_
**Storage** | Stores items | 0 | 24 |
**Peddler** | No need for oars | 2 | 0 | _Fropjet_
**Extendable bridge** | Medium reach and extension speed | 1 | 0 | _Holobridge_
**Steam engine** | Burns _frop_, replaces some manpower | 0 | 8 |  _Combustion engine_
**Lever** | Controls steam engine along a straight line | 1 | 0 | Cable

#### Advanced

Name | Function | Occupants | Inventory
---|---|---|---
**Spire** | Stronger than _abode_, stacks to 3  | 8 | 4 |
**Dynamo** | Creates energy | 0 | 0 |
**Capacitor** | Stores energy | 0 | 100
**Oil tank** | Stores _frop_ oil | 0 | 50
**Combustion engine** | Burns _frop_ oil | 0 | 0
**Holobridge** | Greatest reach, instant deactivation and can be aimed | 0 | 0 | _Teleporter_
**Tumbler** | Allows heavy equipment to move to the other side of a platform | 1 | 0
**Factory** | Versatile sturdy workshop | 3 | 12
**Fropjet** | Propulsion, driven | 0 | 0 |
**Wreckapult** | Even destroys ground (with ground)
**Stormtap** | Catches energy from lightning | 0 | 0
**Folded space** | Massive storage and achieved with spacial mirroring | 10 | 80
**Teleporter** | Basically instant and selective bridge to and from any point within radius | 0 | 0 | _Teleporter_
**Beacon** | Provides light on the dark side of platforms, for _frop_ cultivation and fearlessness | 0 | 0 |

![](ui_mockup.svg)