<!--Ж-universe consists of four planes:

- One god-player, optional number of bots. Can be edited (strange fenomenon)
- One god-player, multiple bots (forces of nature and factions). Story mode.
- Infinite human god-players
- Infinite human kekel-players and kekel-bots
-->

# The World | Muki | Gula

## Gravitational shells

Gravity wells are distributed as quantized concentric triacontahedral shells around the glowing remnants of the planets core. Temperature plays a role in the shell that matter prefers, which - through research - can also be utilized to ascend and descend (flight and diving) with certain vehicles or entire platforms. The story of [Ikalo](ikalo.md) is a cautionary tale about anger that simultaneously provides insight into this physical phenomenon.

The temperature of the matter in these shells roughly matches up to those associated with the physical states of water, and from the center outward they can described as such:

1. Dot singularity
1. Plasma core
1. Vapor shell
1. Temperate shell
1. Frosty shell
1. 0K barrier

Liquid water forms a triacontrahedric film and the atmosphere resettled on each side of this surface. At the icosahedric extremes, rock is pressed together to a boiling point forming volcanoes that hurl glowing rocks into their surroundings. The heat of the volcanoes causes water to vaporize which drifts away over to the dodecahedric points, where they form clouds, that produce rain and thunder.

## The Core

The hottest part of this world, shines like a star and provides the energy that breathes life into the otherwise inorganic remnants of the pulverized and incinerated planet that was.

## Temperate shell

Most matter - and with it, water and life - occupies what may be considered the temperate shell. A faceted bubble of rhomboid ocean membranes connects twelve volcanoes at the icosahedral points. At each of the other corners of the world - the dodecahedral ones - there hovers a constant thunderous cyclone. These phenomena result at the meeting points of the switching cosmological flow along the triacontahedral edges of the gravity wells. Both are highly destructive, but waiting to be tapped. The calm in-between is full of earth fragments. Floating platforms containing all the ingredients for a lush and diverse ecosystem. Irregular chunks stick together over time and are recycled over time as they merge with a volcano and increase the pressure within it. The platforms can also be reshaped by digging 

These islands can be considered to have an inside that is lit by the core and a dark outside.

### Inside

The inside - where photosynthesis is possible - is covered in grass, shrubs and trees. Bug-like creatures (in shape and role somewhat reminiscent of Earth vertebrates) live off the plants and eachother. The Ekelplitfkin are one of the few vertibrate species in this world.

- [Ekelplitfkin](ekelplitfkin.md)
- Light
- Frop
- Trees
- Insects shaped like vertebrates
	
### Outside

- [Gogultuvol](gogultuvol.md)
- Darkness
- Mushrooms
- Nightmarish photophobic creatures
- Hostile
- Underwater-like
- Bioluminescence



### Both sides

- Gems
    + Lizardite (haste and energy)
    + Ruby (fire and heat)


## Frosty shell

Since temperature influences the gravity shell that matter is attracted to, ice litters the shell halfway out from the temperate zone.

## 0K barrier

There is so little energy that reaches the zero Kelvin barrier that once once matter reaches it, it naturally stays there forever. Through advanced laser and plasmoid technology it is possible however to extract materials from this superconductive shell.

## Back to reality

It becomes clear that the world of Mu was created with a single purpose. As more and more is understood about its structure, theories emerge about what would happen if one were to connect the supercharged dot singularity directly to the 0K barrier. Some of the brightest minds hypothesize their world is basically a single-shot interdimensional jump mechanism designed by the Creator's subconscious. The volcanoes at the icosahedral points - according to this theory - are effectively extension cord reels.


A hypothetical conversation between a Kekelplithf and a Gogoltof physicist:
K: "Light comes from above, right?"
G: "It certainly does not! It most definitely comes from below!"
K: "Let's agree to disagree on that one. Still it comes from within, right?"
G: "That too, but it mostly flows out"
