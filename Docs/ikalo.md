# The story of Ikalo

There are many variations to this story that mostly differ in the reason Ikalo got so mad.

There once was an Ekelplitf called Ikalo who got very angry about something. He got so angry even that he heated up to the point that he shot straight into the core. Since, as all [Muyin](muyin.md) know, things that are hot naturally prefer to occupy a smaller gravitational shell.