Togulgabbel are masters of magic and shapeshifters that live on the unlit outside of what they call Gulas. In their language of ... this means _pyramids_. Referring to the volume of the world, seen as a collection of rhomboid 

Their main crop is a kind of mushroom that allows them to take on different forms. Changing shape takes time and costs much stamana, which is also required to move fast, spells and other abilities.

The forms they can take are:

- Migrant
- Former
- Harvester/Mover
- Tank
- Support

The Migrant  

* Morphtime: Short
* Moves: Very fast
* Armor: Some
* Attack: None
* Special: Flying in light

The Former

* Morphtime: Long
* Moves: Fast
* Armor: Little
* Attack: None
* Special: Grows plants into structures and repairs them with magic

The Mover

* Morphtime: Medium
* Moves: Slow
* Armor: Some
* Attack: Lift
* Special: Moves ground

The Tank

* Morphtime: Long
* Moves: Very slow
* Armor: Lots
* Attack: Short-ranged Heavy
* Special: 

The Support

* Morphtime: Very long
* Moves: Medium
* Armor: Some
* Attack: Medium-ranged 
* Special: Unit healer and structure recharger

Structures:

* Mold -> Blimphouse, heals garisoned units and summons new Gogoltovs with frop
* Mushroom -> Puffer, stimulates mushroom growth
* Vine -> Wallbridge, allows non-flying forms to cross to other platforms.
* Pepper -> Thrusters
* Frop -> Froprella, provides shade on the light side, allows for planting structures
