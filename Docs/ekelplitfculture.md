# Ekelplitf culture

Their leaders wear their kilts as headgear on portraits to show off they're privilege of chilling in the bathtub all day. It is considered common sense that the best decisions are made in safe and comfy waters.

There is an Ekelplitfian word board game that involves sliding rhomboid letter tiles that start out as the 5x5 "alphabet". Each turn a player picks a tile from the edge and slides it in at the opposing end, shifting the row. Points are awarded for any new words that are formed by this move.

![](baduki_alphabet.png)