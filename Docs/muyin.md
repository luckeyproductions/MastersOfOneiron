[Ekelplitfkin](ekelplitfkin.md) | [Gogultuvol](gogultuvol.md)
---|---
Slow on land but fast in between platforms because of jetfins.|Fast on platforms, slow in between but flying.
They are afraid of the dark and need a light source or a night vision device|They get sunburn from the coreglow when not anointed or otherwise protecting their skin.
Their crops need watering. It occasionally rains, but having someone water them actively - and later Warka tower irrigation - increases yield. |


