# Science of the Ekelplitfkin

The technological ages are named after the focus of their major advances. During the grogawa or _ground age_ most new ideas had to do with digging and basic mastering of the environment. At the end of the grogawa agriculture emerged and we enter the Fropgawa. The age that follows after that is centered around the application of a newly discovered cellulose plastic and ends with the invention of the combustion engine. During the final age magic returns from the sideline with a vengeance as it merges with science.

## Grogawa -=- ground (stone) age

- Hunting
- Writing
- Digging
- Burrows
- Chill pool
- Workbench
- Basic tools
- Carpentry
- Ramps
- Rowing
- Oar blade
- Husbandry
- Mechanics
- Agriculture

## Fropgawa -=- agricultural age

- Workshop
- Storage
- Bath
- Waterwheel
- Drawbridge
- Grinding
- Smoking
- Weaving
- Oven
- Baking
- Pottery
- Paper
- Glass
- Brewing
- Steam power
- Bubble bath
- Chemistry

## Toligawa -=- cellulose age

- Factory
- Pressing
- Cellulose
- Frop oil
- Durable machines
- Hot tub
- Lenses
- Gasses
- Pneumatics
- Crystallography
- Combustion engine
- Propeller
- Tractor

## Yoyogawa -=- magic age

- Gem mending
- Advanced machines
- Energy tapping
- Computing
- Sensory deprivation tank
- Mechanization
- 3D printing
- Light trap (dark halo powerplant)
- Plasmoid gun
