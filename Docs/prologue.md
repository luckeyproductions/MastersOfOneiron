
# Prologue

Somewhere, a planet is divided into two nations of about the same size. Both leaders of each of these hemispheres are presented with the very same latest top-secret destructive technology designed by their [AI](ai.md)s (that were created to kill). The _asymptotic stability warper_ turns iron into a highly unstable element within a targeted region, instantly and without prior warning.

You never know when you might need it. But at soon as the deal is made and the remote handed over, they are told the same deal is being made with the other. With no chance of retaliation and the guaranteed instant elimination of the other, neither of the leaders hesitate to push the button as soon as it is installed. Compelled to survive, two buttons are pressed, synchronous like clockwork, and the planet explodes in a way that defies all the laws of physics that were _ever_ conceived.

The resulting blast travels so fast that time escapes into the imaginary, dimming the stars and sending the exploding planet into the realm of the hypothetical, increasing the influence spirit has on matter. On top of that all souls have been instantly united with the planet's destruction. Realizing what is happening, this newborn world soul utilizes its increased influence in an attempt to keep this precious combination of elements together, as it sees the only alternative is eternal loneliness.

Applying the strongest cyclotomic field it can imagine, _the spirit_ is able to contain the exploding planet within a triacontahedric configuration, effectively halting the explosion by bending space and turning the crawling blast into a steady cyclical geometric flow. As long as the field is maintained, there may be a chance of restoring the planet, and a familiar plurality of conciousness. But the spirit knows that first it must be patient. With a bit of luck, new lifeforms will emerge on this [world](world.md), able to assist in returning to the real.
