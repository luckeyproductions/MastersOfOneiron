# Masters of Oneiron

Welcome to the [world](world.md) of Oneiron; a spirit dream world, the afterlife and fantasy realm, the inside of black holes, along the imaginary axis of time... an inverted universe where a pocket of space - containing an exploding world - ends up after the [detonation](prologue.md) of a inconceivably destructive weapon that defies the known laws of physics.

