SOURCES += \
    $$PWD/mastercontrol.cpp \
    $$PWD/tile.cpp \
    $$PWD/slot.cpp \
    $$PWD/platform.cpp \
    $$PWD/oneirocam.cpp \
    $$PWD/inputmaster.cpp \
    $$PWD/controllable.cpp \
    $$PWD/spawnmaster.cpp \
    $$PWD/sceneobject.cpp \
    $$PWD/resourcemaster.cpp \
    $$PWD/grass.cpp \
    $$PWD/frop.cpp \
    $$PWD/luckey.cpp \
    $$PWD/world.cpp \
    $$PWD/ekelplitf.cpp \
    $$PWD/volcano.cpp \
    $$PWD/storm.cpp

HEADERS += \
    $$PWD/mastercontrol.h \
    $$PWD/tile.h \
    $$PWD/slot.h \
    $$PWD/platform.h \
    $$PWD/oneirocam.h \
    $$PWD/inputmaster.h \
    $$PWD/controllable.h \
    $$PWD/spawnmaster.h \
    $$PWD/sceneobject.h \
    $$PWD/resourcemaster.h \
    $$PWD/grass.h \
    $$PWD/frop.h \
    $$PWD/luckey.h \
    $$PWD/world.h \
    $$PWD/ekelplitf.h \
    $$PWD/volcano.h \
    $$PWD/storm.h
