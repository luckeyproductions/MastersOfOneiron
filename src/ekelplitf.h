/* Masters of Oneiron
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef IMP_H
#define IMP_H

#include <Dry/Dry.h>

#include "sceneobject.h"

namespace Dry {
class Drawable;
class Node;
class Scene;
class Sprite;
}

using namespace Dry;

class Ekelplitf : public SceneObject
{
    DRY_OBJECT(Ekelplitf, SceneObject);
public:
    Ekelplitf(Context *context);
    static void RegisterObject(Context* context);
    virtual void OnNodeSet(Node* node);
    virtual void Set(Vector3 position, Node* parent);

    virtual void Start();
    virtual void Stop();
private:
    void Update(float timeStep) override;
    AnimatedModel* bodyModel_;
    HashMap<int, AnimatedModel*> equipment_;
    AnimationController* animCtrl_;
    float randomizer_;

    void HandleAnimationFinished(StringHash eventType, VariantMap& eventData);
};

#endif // IMP_H
