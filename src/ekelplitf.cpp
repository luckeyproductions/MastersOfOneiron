/* Masters of Oneiron
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ekelplitf.h"

void Ekelplitf::RegisterObject(Context *context)
{
    context->RegisterFactory<Ekelplitf>();
}

Ekelplitf::Ekelplitf(Context* context):
    SceneObject(context)
{
}

void Ekelplitf::OnNodeSet(Node *node)
{ if (!node) return;

    bodyModel_ = node_->CreateComponent<AnimatedModel>();
    bodyModel_->SetModel(RESOURCE->GetModel("Ekelplitf"));
    bodyModel_->SetMaterial(RESOURCE->GetMaterial("Skin"));
//    bodyModel_->SetCastShadows(true);

    for (int e{0}; e < 3; ++e) {

        AnimatedModel* equipment{ node_->CreateComponent<AnimatedModel>() };

        switch (e) {
        case 0:
            equipment->SetModel(RESOURCE->GetModel("Kilt"));
            break;
        case 1:
            equipment->SetModel(RESOURCE->GetModel("Armour"));
            break;
        case 2:
            equipment->SetModel(RESOURCE->GetModel("ShieldBlade"));
            break;
        }

        Model* model{ equipment->GetModel() };
        if (model) {
            for (unsigned m{0}; m < model->GetNumGeometries(); ++m) {
                switch(m) {
                case 0: equipment->SetMaterial(m, RESOURCE->GetMaterial("Cloth"));
                    break;
                case 1: equipment->SetMaterial(m, RESOURCE->GetMaterial("Plastic"));
                    break;
                case 2: equipment->SetMaterial(m, RESOURCE->GetMaterial("Stretch"));
                    break;
                case 3: equipment->SetMaterial(m, RESOURCE->GetMaterial("Glow"));
                    break;
                default:
                    break;
                }
            }
            equipment_[e] = equipment;
        }
    }

    animCtrl_ = node_->CreateComponent<AnimationController>();
    animCtrl_->PlayExclusive("Animations/StandUp.ani", 1, false);

    SubscribeToEvent(E_ANIMATIONFINISHED, DRY_HANDLER(Ekelplitf, HandleAnimationFinished));
}

void Ekelplitf::Set(Vector3 position, Node *parent)
{
    node_->SetParent(parent);
    node_->SetRotation(Quaternion::IDENTITY);

    SceneObject::Set(position);

    randomizer_ = Random(0.5f, 0.75f);
    node_->Rotate(Quaternion(0.0f, Random(360.0f), 0.0f));
    node_->SetScale(Random(0.023f, 0.023f));

    animCtrl_->SetSpeed("Animations/StandUp.ani", 0.5f + randomizer_);
}

void Ekelplitf::Start()
{
}
void Ekelplitf::Stop()
{
}

void Ekelplitf::Update(float timeStep)
{ (void)timeStep;
}

void Ekelplitf::HandleAnimationFinished(StringHash eventType, VariantMap& eventData)
{ (void)eventType; (void)eventData;

    if (static_cast<Node*>(eventData[AnimationFinished::P_NODE].GetPtr()) == node_)
    animCtrl_->SetSpeed("Animations/StandUp.ani", -animCtrl_->GetSpeed("Animations/StandUp.ani"));
}
