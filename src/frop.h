/* Masters of Oneiron
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef FROP_H
#define FROP_H

#include <Dry/Dry.h>

#include "sceneobject.h"

namespace Dry {
class Drawable;
class Node;
class Scene;
class Sprite;
}

using namespace Dry;

class Frop : public SceneObject
{
    DRY_OBJECT(Frop, SceneObject);
public:
    Frop(Context *context);
    static void RegisterObject(Context* context);
    void Update(float timeStep) override;

    virtual void OnNodeSet(Node* node);
    virtual void Set(Vector3 position, Node *parent);
private:
    StaticModel* fropModel_;
    Vector3 scale_;

    double growthStart_;

    double age_ = 0.0;
};

#endif // FROP_H
