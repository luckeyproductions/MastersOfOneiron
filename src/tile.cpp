/* Masters of Oneiron
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "tile.h"
#include "ekelplitf.h"
#include "frop.h"
#include "grass.h"
#include "platform.h"

void Tile::RegisterObject(Context *context)
{
    context->RegisterFactory<Tile>();
}

Tile::Tile(Context *context):
SceneObject(context),
  health_{1.0f}
{

}

void Tile::OnNodeSet(Node *node)
{ if (!node) return;

    node_->AddTag("Platform");
    node_->AddTag("Tile");

//    StaticModel* centerModel{ node_->CreateComponent<StaticModel>() };
//    centerModel->SetModel(RESOURCE->GetModel("Terrain/Center_1"));
//    centerModel->SetMaterial(RESOURCE->GetMaterial("VCol"));
//    centerModel->SetCastShadows(true);

    //Set up compound nodes.
    for (int i{0}; i < TE_LENGTH; ++i) {
        elements_[i] = node_->CreateChild("TilePart");
        elements_[i]->AddTag("Platform");
        elements_[i]->SetPosition(ElementPosition(static_cast<TileElement>(i)));
    }

//    SubscribeToEvent(E_PHYSICSPOSTSTEP, DRY_HANDLER(Tile, HandleFixedUpdate));
}
Vector3 Tile::ElementPosition(TileElement element)
{
    return Vector3(0.5f - (element / 2),
                   0.0f,
                   0.5f - (element % 2));
}


void Tile::Set(const IntVector2 coords, Platform *platform)
{
    coords_ = coords;
    platform_ = platform;

    node_->SetParent(platform_->GetNode());
    node_->SetRotation(Quaternion::IDENTITY);

    SceneObject::Set(platform_->CoordsToPosition(coords));

    platform_->AddNodeInstance("Terrain/Center_1", node_);

    //Increase platform mass
    platform_->rigidBody_->SetMass(platform_->rigidBody_->GetMass() + 1.0f);

    //Add collision shape to platform
    collider_ = platform_->GetNode()->CreateComponent<CollisionShape>();
    collider_->SetBox(Vector3(1.0f, 2.0f, 1.0f),
                            node_->GetPosition());

    //Create random tile addons
    int extraRandomizer{ Random(23) };

    //Create a dreamspire
    if (extraRandomizer == 7) {
        Node* spireNode{ node_->CreateChild("Spire") };
        spireNode->Translate(Vector3::UP * PLATFORM_HALF_THICKNESS);
        if (coords_.x_ % 2) spireNode->Rotate(Quaternion(180.0f, Vector3::UP));
        StaticModel* model{ spireNode->CreateComponent<StaticModel>() };
        model->SetModel(RESOURCE->GetModel("Abode"));
        model->SetMaterial(0, RESOURCE->GetMaterial("Abode"));
        model->SetCastShadows(true);
    }
    //Create Ekelplitfs
    else if (extraRandomizer < 7){
        int totalEkelplitfs{Random(0, 2)};
        for (int i{0}; i < totalEkelplitfs; ++i){
            Vector3 position{ Vector3(-0.075f * totalEkelplitfs + 0.15f * i, PLATFORM_HALF_THICKNESS, Random(-0.25f, 0.25f)) };
            SPAWN->Create<Ekelplitf>()->Set(position, node_);
        }
    }
    //Create fire
    else if (extraRandomizer == 8){
        Node* fireNode{ node_->CreateChild("Fire") };
        fireNode->Translate(Vector3::DOWN * PLATFORM_HALF_THICKNESS * 2.0f);
        ParticleEmitter* particleEmitter{ fireNode->CreateComponent<ParticleEmitter>() };
        ParticleEffect* particleEffect{ CACHE->GetResource<ParticleEffect>("Resources/Particles/Fire.xml") };
        particleEmitter->SetEffect(particleEffect);
        Light* fireLight{fireNode->CreateComponent<Light>()};
        fireLight->SetRange(2.3f);
        fireLight->SetColor(Color(1.0f, 0.88f, 0.666f));
        fireLight->SetCastShadows(true);
        particleEffect->SetRelative(true);
    }
    //Create frop crops
    else if (extraRandomizer > 8 && coords.x_ % 2 == 0){
        for (int i{0}; i < 4; ++i){
            for (int j{0}; j < 3; ++j){
                    Vector3 position{ Vector3(-0.375f + i * 0.25f, PLATFORM_HALF_THICKNESS, -0.3f + j * 0.3f) };
                    SPAWN->Create<Frop>()->Set(position, node_);
            }
        }
        for (int i = 0; i < extraRandomizer - 8; i++){
            Vector3 randomPosition{ Vector3(Random(-0.4f, 0.4f), PLATFORM_HALF_THICKNESS, Random(-0.4f, 0.4f)) };
            SPAWN->Create<Frop>()->Set(randomPosition, node_);
        }
    }

}

void Tile::Disable()
{
    collider_->SetEnabled(false);

    platform_->rigidBody_->SetMass(platform_->rigidBody_->GetMass() - 1.0f);
}

void Tile::Start()
{
}
void Tile::Stop()
{
}

void Tile::FixedUpdate(float timeStep)
{
    Input* input{GetSubsystem<Input>()};

    bool up{ input->GetKeyDown(KEY_UP) };
    bool down{ input->GetKeyDown(KEY_DOWN) };
    bool left{ input->GetKeyDown(KEY_LEFT) };
    bool right{ input->GetKeyDown(KEY_RIGHT) };

    if (buildingType_ == B_ENGINE) {
        if (true)//up
//         || (left && node_->GetPosition().x_ > 0.0f)
//         || (right && node_->GetPosition().x_ < 0.0f))
        {
            platform_->rigidBody_->ApplyForce(timeStep * platform_->GetNode()->GetDirection() * (5000.0f + 4000.0f * MC->Sine(0.23f, -1.0f, 1.0f) * Sqrt(node_->GetPosition().x_)), node_->GetPosition());
        }
        else if (down
                || (right && node_->GetPosition().x_ > 0.0f)
                || (left && node_->GetPosition().x_ < 0.0f))
        {
            platform_->rigidBody_->ApplyForce(-platform_->GetNode()->GetDirection() * 5000.0f * timeStep, node_->GetPosition());
        }
    }
}

void Tile::SetBuilding(BuildingType type)
{
    buildingType_ = type;
    if (buildingType_ > B_EMPTY) platform_->DisableSlot(coords_);
//    StaticModel* model{ node_->GetComponent<StaticModel>() };
    switch (buildingType_)
    {
    case B_ENGINE: {
        StaticModel* model{ node_->GetOrCreateComponent<StaticModel>() };
        model->SetModel(RESOURCE->GetModel("Buildings/HyperEngine"));
        model->SetMaterial(0, RESOURCE->GetMaterial("Plastic"));
        model->SetMaterial(1, RESOURCE->GetMaterial("Glow"));
    } break;
    default: break;
    }
}

BuildingType Tile::GetBuilding()
{
    return buildingType_;
}

void Tile::FixFringe()
{
    for (int e{0}; e < TE_LENGTH; ++e) {

        Node* element{ elements_[e] };
//        StaticModel* model{ element->GetComponent<StaticModel>() };

        CornerType cornerType{ platform_->PickCornerType(coords_, static_cast<TileElement>(e)) };

        switch (cornerType) {
        case CT_NONE:
            platform_->RemoveNodeInstance(element);
            break;
        case CT_IN: {
            platform_->AddNodeInstance("Terrain/BendIn_" + String(Random(1, 5)), element);

//            model->SetModel(RESOURCE->GetModel("Terrain/BendIn_" + String(Random(1, 5))));

            switch (e) {
            case TE_NORTHEAST:
                elements_[e]->SetRotation(Quaternion(0.0f, 180.0f, 0.0f));
                break;
            case TE_SOUTHEAST:
                elements_[e]->SetRotation(Quaternion(0.0f, -90.0f, 0.0f));
                break;
            case TE_SOUTHWEST:
                elements_[e]->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
                break;
            case TE_NORTHWEST:
                elements_[e]->SetRotation(Quaternion(0.0f, 90.0f, 0.0f));
                break;
            }
        } break;
        case CT_OUT: {
            platform_->AddNodeInstance("Terrain/BendOut_" + String(Random(1, 5)), element);

//                model->SetModel(RESOURCE->GetModel("Terrain/BendOut_" + String(Random(1, 5))));

                switch (e) {
                case TE_NORTHEAST:
                    elements_[e]->SetRotation(Quaternion(0.0f, 180.0f, 0.0f));
                    break;
                case TE_SOUTHEAST:
                    elements_[e]->SetRotation(Quaternion(0.0f, -90.0f, 0.0f));
                    break;
                case TE_SOUTHWEST:
                    elements_[e]->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
                    break;
                case TE_NORTHWEST:
                    elements_[e]->SetRotation(Quaternion(0.0f, 90.0f, 0.0f));
                    break;
                }
        } break;
        case CT_STRAIGHT: {
            platform_->AddNodeInstance("Terrain/Straight_" + String(Random(1, 5)), element);
//                model->SetModel(RESOURCE->GetModel("Terrain/Straight_" + String(Random(1, 5))));

                switch (e) {
                case TE_NORTHEAST:
                    elements_[e]->SetRotation(Quaternion(0.0f, 180.0f, 0.0f));
                    break;
                case TE_SOUTHEAST:
                    elements_[e]->SetRotation(Quaternion(0.0f, -90.0f, 0.0f));
                    break;
                case TE_SOUTHWEST:
                    elements_[e]->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
                    break;
                case TE_NORTHWEST:
                    elements_[e]->SetRotation(Quaternion(0.0f, 90.0f, 0.0f));
                    break;
                }
        } break;
        case CT_BRIDGE: {
            platform_->AddNodeInstance("Terrain/Bridge_1", element);

//            model->SetModel(RESOURCE->GetModel("Terrain/Bridge_1"));

            switch (e) {
            case TE_NORTHEAST: case TE_SOUTHWEST:
                elements_[e]->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
                break;
            case TE_SOUTHEAST: case TE_NORTHWEST:
                elements_[e]->SetRotation(Quaternion(0.0f, 90.0f, 0.0f));
                break;
            }
        } break;
        case CT_FILL: {
            if (e == TE_NORTHEAST)
                platform_->AddNodeInstance("Terrain/Fill_1", element);

//                model->SetModel(RESOURCE->GetModel("Terrain/Fill_1"));
            else
                platform_->RemoveNodeInstance(element);

        } break;
        default: break;
        }
//        if (model->GetModel())
//            model->SetMaterial(RESOURCE->GetMaterial("VCol"));
    }
}
